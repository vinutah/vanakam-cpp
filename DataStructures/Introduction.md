# Fixed-size data structures
    * One-dimensional arrays
    * Two-dimensional arrays

# Dynamic data structures
    * Grow and Shrink during execution

## Linked Lists
    * Collections of data items
    * Logically lined up in a row
    * Insertions and removals are made anywhere

## Stacks
    * Important in compilers and operating systmes
    * Insertions and removals are made only at
      one end of stack -- its top

## Queues
    * Represent waiting lines
    * Insertions are made at the back - tail
    * Removals are made from the front -- head

## Binary Trees
    * Facilitate high-speed searching and sorting
      of data
    * Efficient elimination of duplicate data items
    * Representation of file-systmes directories and compilation
      of expressions into machine language